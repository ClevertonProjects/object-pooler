using System.Collections.Generic;
using UnityEngine;

/*! /class ObjectPooler
 *  /brief Manages a pool of objects to reuse instead of instantiate new objects.
 */
public class ObjectPooler : MonoBehaviour
{
    private static ObjectPooler instance;

    public static ObjectPooler Instance
    {
        get { return instance; }
    }

    /// Sub class container of every object type.
    [System.Serializable]
    class PoolTypes
    {
        [SerializeField] private GameObject objectPrefab;                               //!< Object to create in the list.
        [SerializeField] private PoolObjectTags tag;                                    //!< Object tag.
        [SerializeField] private int amountToPool;                                      //!< Amount of objects to create in the list.
        [SerializeField] private bool shouldExpand;                                     //!< Check if the list can grow if necessary.

        public GameObject ObjectPrefab { get { return objectPrefab; } }
        public int AmountToPool { get { return amountToPool; } }
        public bool ShouldExpand { get { return shouldExpand; } }

        public PoolObjectTags Tag { get { return tag; } }
    }    

    [SerializeField] private PoolTypes[] poolTypes;                                 //!< Objects to create in the pool.    
    private List<PoolItem> poolList;                                                //!< List of created items.

    private void Awake()
    {
        instance = this;
        StartPool();
    }

    /// Creates the objects to the pool list.
    private void CreatePooledObjects (int amountToCreate, GameObject objectToCreate)
    {        
        Transform container = transform;

        for (int i = 0; i < amountToCreate; i++)
        {
            GameObject go = Instantiate(objectToCreate, container);
            go.SetActive(false);
            poolList.Add(go.GetComponent<PoolItem>());
        }
    }

    /// <summary>
    /// Initializes the pool list.
    /// </summary>
    public void StartPool ()
    {
        poolList = new List<PoolItem>();

        for (int i = 0; i < poolTypes.Length; i++)
        {
            CreatePooledObjects(poolTypes[i].AmountToPool, poolTypes[i].ObjectPrefab);
        }
    }

    /// <summary>
    /// Returns the first available GameObject in the pool list.
    /// </summary>    
    public GameObject GetPooledObject (PoolObjectTags tag)
    {
        int tagValue = (int)tag;

        for (int i = 0; i < poolList.Count; i++)
        {
            if (!poolList[i].IsActive && poolList[i].IsTagEqual(tagValue))
            {
                return poolList[i].GO;
            }
        }       

        foreach (PoolTypes item in poolTypes)
        {
            if (item.Tag == tag)
            {
                if (item.ShouldExpand)
                {
                    CreatePooledObjects(1, item.ObjectPrefab);
                    return poolList[poolList.Count - 1].GO;
                }
            }
        }           

        return null;
    }
}
