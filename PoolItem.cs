﻿using UnityEngine;

/*! \class PoolItem
 *  \brief Pooled item data.
 */
public class PoolItem : MonoBehaviour
{
    [SerializeField] private PoolObjectTags tag;                                //!< Pool tag.
    [SerializeField] private GameObject myGO;                                   //!< Reference to the GameObject.

    public bool IsTagEqual (int tagNumber)
    {
        return (int)tag == tagNumber;
    }
    
    public GameObject GO
    {
        get { return myGO; }
    }

    public bool IsActive
    {
        get { return myGO.activeSelf; }
    }
}
